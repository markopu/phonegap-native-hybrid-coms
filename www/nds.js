module.exports = {
    // data == receiver intent name
    init: function (data, successCallback, errorCallback) {
	cordova.exec(successCallback, errorCallback, "NDS", "init", [data]);
    },

    // receiver callback
    setCallback: function (successCallback, errorCallback) {
	cordova.exec(successCallback, errorCallback, "NDS", "setCallback", []);
    },

    // data to be sent to other application {0: <intent_action>, 1: <data>}
    sendData: function (data, successCallback, errorCallback) {
	cordova.exec(successCallback, errorCallback, "NDS", "sendData", data);
    }
}
