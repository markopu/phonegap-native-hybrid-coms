package com.example.plugin;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NDS extends CordovaPlugin {
    // receiver callback - data will be avaliable here
    private CallbackContext callback = null;

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) 
	throws JSONException {
	
	if (action.equals("init")) {
	    // get package name and register intent filter
	    // data[0] holds intent name
	    String receiverIntentName = data.getString(0);
	    String packageName = getActivity().getApplicationContext().getPackageName();
	    
	    // add intent filter
	    initCustomEvent(packageName, receiverIntentName, broadcastReceiverCustom);
	    
	    callbackContext.success(packageName + "." + receiverIntentName);

	    return true;
	} else if (action.equals("setCallback")){
	    // set callback that will receive data on broadcast received
	    callback = callbackContext;

	    return true;
	} else if (action.equals("sendData")) {
	    Context context = getActivity().getApplicationContext();
	    Intent intent = new Intent();
	
	    /* 
	     * data: JSONArray {0: <intent_action>, 1: <data>}
	     */
	    String intentAction = data.getString(0);
	    String dataString = data.getString(1);

	    intent.setAction(intentAction);
	    intent.putExtra("extra", dataString);
	    context.sendBroadcast(intent);

	    callbackContext.success("Sent Broadcast intent.");

	    return true;
	} else {
	    return false;
	}
    }

    private Activity getActivity() {
	return this.cordova.getActivity();
    }

    // create intent filter
    public void initCustomEvent(String packageName, String intentName, BroadcastReceiver br) {
	IntentFilter filterCustom = new IntentFilter(packageName + "." + intentName);
	getActivity().getApplicationContext().registerReceiver(br, filterCustom);
    }

    // broadcast receiver that listens for events on packageName.intentName
    BroadcastReceiver broadcastReceiverCustom = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
		if (callback != null) {
		    try {
			// TODO: parse data within the intent
			String receivedData = intent.getExtras().getString("extra");
			JSONObject parameter = new JSONObject();

			parameter.put("extra", receivedData);

			// send data via receiver callback to the main app
			PluginResult result = new PluginResult(PluginResult.Status.OK, parameter);
			result.setKeepCallback(true);
			callback.sendPluginResult(result);

			// switch back to phonegap application
			Intent i = new Intent();
			String packageName = context.getPackageName();
			i.setClassName(packageName, packageName + ".MainActivity");
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		    } catch (JSONException e) {
			// TODO: error handling
		    }
		}
	    }
	};
}
